﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args) {
    	
    	
    	while(true) {				//Endlosschleife, damit man immer wieder Tickets kaufen kann
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
            double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
            fahrkartenAusgeben();
            rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
    	}

    }
    
    //Hier die Methoden rein
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
    			+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
    			+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
    			+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
    	
    	System.out.print("Ihre Wahl: ");
    	int ticketEingabe = tastatur.nextInt();
    	
    	
    	
    	switch(ticketEingabe) {
    	
    	case 1:
    		zuZahlenderBetrag = 2.90;
    		break;
    	case 2:
    		zuZahlenderBetrag = 8.60;
    		break;
    	case 3:
    		zuZahlenderBetrag = 23.50;
    		break;
    	default:
    		System.out.println(">>falsche Eingabe<<");
    	}
    	
        System.out.print("Anzahl der Tickets: ");
        int anzahlTickets = tastatur.nextInt();
        
        if (anzahlTickets >= 1 && anzahlTickets <= 10) {
        	zuZahlenderBetrag = zuZahlenderBetrag*anzahlTickets;
        }
        
        else {
        	System.out.println("Falsche Eingabe. Es wird mit einem Ticket weitergerechnet.");
        	anzahlTickets = 1;
        	zuZahlenderBetrag = zuZahlenderBetrag*anzahlTickets;
        }

        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	// Geldeinwurf
        // -----------
    	Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %1.2f EURO\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;		//eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneMünze
        }
        return eingezahlterGesamtbetrag;
    	
    }
    
    public static void fahrkartenAusgeben() {
    	// Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	// Rückgeldberechnung und -Ausgabe
        // -------------------------------
        double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %1.2f" + " EURO ", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n");
    }
}