
public class Datentypen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// DATENTYPEN
		
		String text = "Holzeisenbahn"; // Zeichenkette
		
		int alter = 19; // Ganzzahl
		
		double zinssatz = 3.22; // Kommazahl
		
		char buchstabe = 'd'; // Zeichen (mit '')
		
		boolean auswahl = true; //Wahrheitswert
		
		/*
		 Ganzzahlen (byte, short, int, long)
		 Gleitkommazahlen (float, double)
		 */
		
		
		char ziffer = '1';
		double grad, rad; //beide Variablen werden sp�ter initialisiert, hier nur Deklaration
		final double pi = 3.14159;
		
		System.out.println("Ziffer + 25 = " + ziffer + 25);
		/*
		 "ziffer" ist ein character, also String, daher wird es vor die 25 gepackt, aber nicht addiert.
		 */
		

		float summe_gehalt = 6;
		System.out.println(summe_gehalt);
		
	}

}
