
public class Datenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.print("Hallo Welt"); // Ausgabe ohne Zeilenvorschub
		
		System.out.println("Hallo Welt"); // Ausgabe mit Zeilenvorschub
		
		System.out.print("Hallo Welt\n"); // Ausgabe mit Zeilenvorschub
		
		System.out.println("Er sagte: \"Guten Tag!\""); // Mit Backslash kann man z.B. Anf�hrungsstriche ausgeben
		
		
		// printf (Methode)
		
		// System.out.printf(format, arguments);
		
		System.out.printf("Hello %s!%n", "World");
		
		/*
		Hier enth�lt die printf-Methode eine Zeichenkette als argument und 2 Formatierungsregeln.
		Die erste Regel (%s) bezieht sich auf den String "World".
		Die zweite Regel (%n) f�gt am Ende der Zeichenfolge einen Zeilenvorschub hinzu.
		Also es k�nnen so viele %-Dinger VOR dem Komma stehen, wie sie wollen, aber nach dem Komma kommen die Sachen,
		die eingesetzt werden sollen, also an die Platzhalter-Stellen.
		 */
		
		/*
		%s = formatiert Zeichenfolgen
		%d = formatiert Dezimalzahlen
		%f = formatiert Flie�kommazahlen
		%t = formatiert Datums-/Uhrzeitwerte
		 */
		
		System.out.printf("Hello %15s!%n", "World");
		
		/*
		Das "%15s" sagt, dass zwischen dem "Hello" und dem "World" 15 Leerstellen Platz gelassen werden.
		 */
		
		System.out.printf("Hello %-15s!%n", "World");
		
		/*
		Das "%-15s" macht die Ausgabe linksb�ndig, 
		 */
		
		System.out.printf("%f %n", 5.1473);
		
		/*
		"%f" formatiert die 5.1473 zu einer float (obwohl die doch schon eine ist?)
		 */
		
		System.out.printf("%5.2f %n", 5.1473);
		
		/*
		Hier ist width = 5 und L�nge = 2
		D.h. die width = 5 gibt an, dass in der Ausgabe 5 Zeichen stehen. Und die L�nge = 2 gibt an, dass die 
		Zahl auf 2 Nachkommastellen gerundet wird.
		Als Ergebnis f�ngt die Ausgabe dann mit einer Leerstelle an, da wir width = 5 gesagt haben und L�nge = 2. Also
		gibt es 2 Nachkommastellen, das Komma und die 5 vor dem Komma. Macht zusammen 4 Zeichen, aber da wir width = 5
		gesagt haben, muss vorher noch eine Leerstelle eingef�gt werden. Bei widht = 6 w�ren dort 2 Leerstellen.
		 */
		
		System.out.printf("|Hallo %10.4f| %n", 199.87795224);
		
		/*
		Hier ist die width = 10 und die L�nge = 4.
		Die width = 10 f�ngt erst an zu z�hlen nach einer Leerstelle nach dem Hallo
		 */
		
		System.out.printf("|Hallo %10.2f| hab ich gemacht %s",  199.9, "Gero");
		
		
		String test = "Java-Programm"; // String definiert
		
		System.out.printf( "\n|%s|\n", test );
		System.out.printf( "|%s| %n", test );
		// Machen beide genau das gleiche
		
		int str1 = 123456789;
		System.out.printf("|%+20d|%n", str1);
		
		String str2 = "0123456789";
		System.out.printf("|%+20s|%n", str2);
		
		System.out.printf("%5.5f", 1.23456789);
	}

}
