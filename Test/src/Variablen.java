
public class Variablen {

	public static void main(String[] args) {
		
		// VARIABLEN
		
		/*
		 Variablen stellen einen Container f�r Daten/Werte (Zahlen, Zeichen, Texte, Objekte) dar.
		 Variablen ben�tigen einen eindeutigen Variablennamen und einen Datentyp (und Speicherplatz, welcher aber
		 automatisch vom System vergeben wird)
		 */
		
		String ausgabe = "Holzeisenbahn";
		
		/*
		 "String" = Datentyp
		 "ausgabe" = Variablenname
		 "Holzeisenbahn" = Wert
		 */
		
		String name; // Deklaration der Variable "name" von Typ String
		name = "Peter"; //Initialisierung der Variable mit einem Wert
		name = "Moritz"; //Zuweisung eines neuen Werts
		
		
		// KONSTANTEN
		
		/*
		 Konstanten stellen Werte dar, die im laufenden Programm nicht mehr ver�ndert werden k�nnen.
		 Sie werden mit dem Schl�sselwort "final" dargestellt.
		 */
		
		final int MWSt = 19;
		
		
		
		System.out.println("test, noch ein test.");
		//hier wird der string ausgegeben und dann mit dem Cursor in die n�chste Zeile gesprungen

		System.out.printf("Hallo %-10s", "Oh1\n");
		//%10s macht rechtsb�ndig, und macht 10 Leerstellen. "%" ist ein PLATZHALTER, und das was danach
		// in Klammern steht wird dann danach eingef�gt
		//durch das \n wird der n�chste printf-Befehl in die n�chste Zeile gesetzt, ABER mit 10 Leerzeichen
		System.out.printf("|Hallo %10.4f|", 199.87795224);
		//%10.4f -> die 4. Nachkommastelle wird gerundet
		System.out.printf("|Hallo %10.2f| hab ich gemacht %s",  199.9, "Gero");
		
		System.out.println("Hallo " + 199.9 + " hab ich gemacht");
		
		System.out.println( 199.9);
		System.out.println(" 199.9");
		
		System.out.printf("%2.5f %n", 123456789.23456789);
		String str2 = "0123456789";
		long anzahlBevoelkerung = 78000000;
	}

}
